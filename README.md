# HomeAssignment

configuration file can be found @ $HomeAssignment/appsettings.json

config details:
- SourceDirectory - the path of the directory which contains the UserExpenses report jsons
- ResultsDirectoy - the path for the directory to save the result file (userExpensesResults_{time}.json)
- ReadFilesParallelism - max parallelism degree for loading the data files 
- ProcessFilesParallelism max parallelism degree for processing the data files
