﻿using HomeAssignment.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;

namespace HomeAssignment
{
    public class UserExpensesFileLoader : IDataSourceLoader<UserExpense>
    {
        private readonly string directoryPath;
        private readonly ParallelOptions parallelOptions;
        private readonly JsonSerializer serializer;
        public BlockingCollection<UserExpense> DataCollection { get; set; }
        
        public UserExpensesFileLoader(IConfiguration config)
        {            
            this.directoryPath = config.GetSection("SourceDirectory").Value;
            int.TryParse(config.GetSection("ReadFilesParallelism").Value, out int parallelDeg);
            this.parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = parallelDeg == 0 ? Environment.ProcessorCount : parallelDeg };
            this.serializer = new JsonSerializer();
            this.DataCollection = new BlockingCollection<UserExpense>();      
        }

        public bool Validate()
        {
            //perform basic validation..
            if (string.IsNullOrWhiteSpace(directoryPath) || !Directory.Exists(this.directoryPath))
            {
                Console.WriteLine("Invalid SourceDirectory config");
                return false;
            }

            return true;
        }

        public Task Load()
        {             
           return Task.Run(() =>
            {
                try
                {                    
                    var filePaths = Directory.GetFiles(directoryPath);
                    Parallel.ForEach(filePaths, this.parallelOptions, file =>
                    {
                        try
                        {
                            using (StreamReader sr = File.OpenText(file))
                            {                                
                                UserExpense userExpense = (UserExpense)this.serializer.Deserialize(sr, typeof(UserExpense));
                                this.DataCollection.Add(userExpense);
                                //success metrics and logging goes here..
                            }
                        }
                        catch (Exception ex)
                        {
                            //failed metrics and logging goes here..
                            Console.WriteLine("Failed to deserialize file: {0}. Exception: {1}", file, ex.Message);
                        }
                    });
                }
                catch (Exception ex)
                {
                    //failed metrics and logging goes here..
                    Console.WriteLine("An error occured while trying reading files from directory: {0}", ex.Message);
                }
                finally
                {
                    this.DataCollection.CompleteAdding();
                }
                //completion metrics and logging goes here..
            });
        }
    }
}
