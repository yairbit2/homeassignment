﻿
using System.Threading.Tasks;

namespace HomeAssignment
{
    public class Pipeline<T>
    {
        private readonly IDataSourceLoader<T> loader;

        private readonly IDataTransformer transformer;
        public Pipeline(IDataSourceLoader<T> loader, IDataTransformer transformer)
        {
            this.loader = loader;
            this.transformer = transformer;
        }

        public void Run()
        {
            // just some basic validations..
            if (!this.loader.Validate() || !transformer.Validate())
            {
                //log and metrics
                return;
            }

            //initialize pipeline
            Task[] pipeline = new Task[]
            {
                 this.loader.Load(),
                 this.transformer.DoWork()
            };
            Task.WaitAll(pipeline);

            //save results and finish
            this.transformer.SaveResults();
        }
    }
}
