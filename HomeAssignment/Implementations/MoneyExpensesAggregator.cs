﻿using HomeAssignment.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;

namespace HomeAssignment
{
    public class MoneyExpensesAggregator : IDataTransformer
    {        
        private readonly IDataSourceLoader<UserExpense> dataSource;        
        private readonly ParallelOptions parallelOptions;
        private readonly string directoryPath;
        private ConcurrentDictionary<string, UserExpense> moneyExpenses;

        public MoneyExpensesAggregator(IConfiguration config, IDataSourceLoader<UserExpense> dataSource)
        {
            this.dataSource = dataSource;
            this.moneyExpenses = new ConcurrentDictionary<string, UserExpense>();
            this.directoryPath = config.GetSection("ResultsDirectoy").Value;
            int.TryParse(config.GetSection("ProcessFilesParallelism").Value, out int parallelDeg);
            this.parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = parallelDeg == 0 ? Environment.ProcessorCount : parallelDeg };                    

            if (!Directory.Exists(this.directoryPath))
            {
                Directory.CreateDirectory(this.directoryPath);
            }
        }

        public bool Validate()
        {
            //perform basic validation..
            if (this.dataSource == null)
            {
                Console.WriteLine("Failed to resolve DataSourceProvider");
                return false;
            }
            
            if (string.IsNullOrWhiteSpace(directoryPath))
            {
                Console.WriteLine("Invalid ResultsDirectoy config");
                return false;
            }

            return true;
        }

        public Task DoWork()
        {
            return Task.Run(() =>
            {
                Parallel.ForEach(dataSource.DataCollection.GetConsumingEnumerable(), parallelOptions, entity =>
                {
                    //this means that nither 'name' or 'category' can contain the pipe char ('|')
                    //could also be done using overriding GetHashCode() and Equals()
                    string key = entity.name + "|" + entity.category; 
                    this.moneyExpenses.AddOrUpdate(key, entity, (k, v) => {
                        v.moneySpent += entity.moneySpent;
                        return v;
                        });
                });                                
            });
        }
        
        public void SaveResults()
        {
            try
            {
                string json = JsonConvert.SerializeObject(this.moneyExpenses.Values);
                string saveTo = this.directoryPath + "/userExpensesResults_" + DateTime.Now.ToString("MMdd_HHmmss") + ".json";
                File.WriteAllText(saveTo, json);
                Console.WriteLine("Results can be found @{0}", saveTo);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to save result file. Exception: {0}", ex.Message);
            }            
        }
    }
}
