﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeAssignment.Model
{
    public class UserExpense
    {
        public string name { get; set; }

        public decimal moneySpent { get; set; }

        public string category { get; set; }       
    }
}
