﻿using HomeAssignment.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace HomeAssignment
{
    class Program
    {
        static void Main(string[] args)
        {         
            var services = ConfigureServices();
            var serviceProvider = services.BuildServiceProvider();
            
            //resolve pipeline(s) and start processing
            serviceProvider.GetService<Pipeline<UserExpense>>().Run();                             

            Console.WriteLine("Done");
            Console.ReadLine();
        }

        static IServiceCollection ConfigureServices()
        {
            IServiceCollection services = new ServiceCollection();
            IConfiguration Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();            

            services.Add(new ServiceDescriptor(typeof(IConfiguration), Configuration));
            services.AddSingleton<IDataSourceLoader<UserExpense>, UserExpensesFileLoader>();
            services.AddTransient<IDataTransformer, MoneyExpensesAggregator>();
            services.AddTransient<Pipeline<UserExpense>>();
            return services;
        }
    }
}
