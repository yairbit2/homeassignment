﻿using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace HomeAssignment
{
    public interface IDataSourceLoader<T>
    {
        BlockingCollection<T> DataCollection { get; }

        bool Validate();

        Task Load();
    }
}
