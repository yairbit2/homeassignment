﻿using System.Threading.Tasks;

namespace HomeAssignment
{
    public interface IDataTransformer
    {       
        bool Validate();

        Task DoWork();

        void SaveResults();
    }
}
